decal PlasmaShock1{
	pic PLSPA0
	fullbright
	add 0.6
	x-scale 0.2
	y-scale 0.2
	randomflipx
	randomflipy
	lowerdecal PlasmaScorchLower
	animator GoAway
}
decal PlasmaShock2{
	pic PLSPB0
	fullbright
	add 0.6
	x-scale 0.2
	y-scale 0.2
	randomflipx
	randomflipy
	lowerdecal PlasmaScorchLower1
	animator GoAway
}
decal PlasmaShock3{
	pic PLSPC0
	fullbright
	add 0.6
	x-scale 0.2
	y-scale 0.2
	randomflipx
	randomflipy
	lowerdecal PlasmaScorchLower1
	animator GoAway
}
decalgroup PlasmaShock{
	PlasmaShock1 1
	PlasmaShock2 1
	PlasmaShock3 1
}

decal GooScorch{
	pic BAL7E0
	x-scale 0.7
	y-scale 0.7
	add 0.6
	lowerdecal Scorch
	animator GoAway2
}

decal BulletScratch{
	pic PUFFD0
	x-scale 0.2
	y-scale 0.2
	randomflipx
	randomflipy
	lowerdecal BulletChip
	add 0.2
}

decal BulletChipSmall1{
	pic CHIP1
	translucent 0.85
	shade "00 00 00"
	x-scale 0.3
	y-scale 0.3
	randomflipx
	randomflipy
}
decal BulletChipSmall2{
	pic CHIP2
	translucent 0.85
	shade "00 00 00"
	x-scale 0.3
	y-scale 0.3
	randomflipx
	randomflipy
}
decal BulletChipSmall3{
	pic CHIP3
	translucent 0.85
	shade "00 00 00"
	x-scale 0.3
	y-scale 0.3
	randomflipx
	randomflipy
}
decal BulletChipSmall4{
	pic CHIP4
	translucent 0.85
	shade "00 00 00"
	x-scale 0.3
	y-scale 0.3
	randomflipx
	randomflipy
}
decal BulletChipSmall5{
	pic CHIP5
	translucent 0.85
	shade "00 00 00"
	x-scale 0.3
	y-scale 0.3
	randomflipx
	randomflipy
}
decalgroup BulletChipSmall{
	BulletChipSmall1	1
	BulletChipSmall2	1
	BulletChipSmall3	1
	BulletChipSmall4	1
	BulletChipSmall5	1
}
decal BulletScratchSmall{
	pic PUFFD0
	x-scale 0.12
	y-scale 0.12
	randomflipx
	randomflipy
	lowerdecal BulletChipSmall
	add 0.2
}


decal BulletChipGiant1{
	pic CHIP1
	translucent 0.85
	shade "00 00 00"
	x-scale 1.4
	y-scale 1.4
	randomflipx
	randomflipy
}
decal BulletChipGiant2{
	pic CHIP2
	translucent 0.85
	shade "00 00 00"
	x-scale 1.4
	y-scale 1.4
	randomflipx
	randomflipy
}
decal BulletChipGiant3{
	pic CHIP3
	translucent 0.85
	shade "00 00 00"
	x-scale 1.4
	y-scale 1.4
	randomflipx
	randomflipy
}
decal BulletChipGiant4{
	pic CHIP4
	translucent 0.85
	shade "00 00 00"
	x-scale 1.4
	y-scale 1.4
	randomflipx
	randomflipy
}
decal BulletChipGiant5{
	pic CHIP5
	translucent 0.85
	shade "00 00 00"
	x-scale 1.4
	y-scale 1.4
	randomflipx
	randomflipy
}
decalgroup BulletChipGiant{
	BulletChipGiant1	1
	BulletChipGiant2	1
	BulletChipGiant3	1
	BulletChipGiant4	1
	BulletChipGiant5	1
}

decal CacoScorch{
	pic BAL2C0
	fullbright
	add 0.8
	x-scale 0.6
	y-scale 0.6
	randomflipx
	randomflipy
	lowerdecal Scorch
	animator GoAway
}

fader BFGAway{
	decaystart 6
	decaytime 10
}
decal BFGSM1{
	pic BFGLITE1
	x-scale 0.4
	y-scale 0.4
	shade "80 80 ff"
	fullbright
	randomflipx
	animator GoAway2
	lowerdecal DoomImpScorch
}
decal BFGSM2{
	pic BFGLITE2
	x-scale 0.4
	y-scale 0.4
	shade "80 80 ff"
	fullbright
	randomflipy
	animator GoAway2
	lowerdecal DoomImpScorch
}
decal HDBFGLightning1{
	pic BFGLITE1
	shade "80 80 ff"
	fullbright
	randomflipx
	animator BFGAway
	lowerdecal BFGScorch
}
decal HDBFGLightning2{
	pic BFGLITE2
	shade "80 80 ff"
	fullbright
	randomflipy
	animator BFGAway
	lowerdecal BFGScorch
}
decalgroup BFGLightningSmall{
	BFGSM1	1
	BFGSM2	1
}
decalgroup HDBFGLightning{
	HDBFGLightning1	1
	HDBFGLightning2	1
}


fader BrontoAway{
	decaystart 0
	decaytime 4
}
decal BrontoCrack1{
	pic BFGLITE1
	x-scale 0.8
	y-scale 0.8
	shade "e0 40 00"
	fullbright
	randomflipx
	randomflipy
	animator BrontoAway
	lowerdecal Scorch
}

decal BrontoCrack2{
	pic BFGLITE2
	x-scale 0.8
	y-scale 0.8
	shade "e0 40 00"
	fullbright
	randomflipx
	randomflipy
	animator BrontoAway
	lowerdecal Scorch
}

decalgroup BrontoScorch{
	BrontoCrack1	1
	BrontoCrack2	1
}

decal BusterScorch1{
	pic BFGLITE1
	shade "e0 40 00"
	fullbright
	randomflipx
	animator BrontoAway
	lowerdecal BFGScorch
}
decal BusterScorch2{
	pic BFGLITE2
	shade "e0 40 00"
	fullbright
	randomflipy
	animator BrontoAway
	lowerdecal BFGScorch
}
decalgroup BusterScorch{
	BusterScorch1	1
	BusterScorch2	1
}
